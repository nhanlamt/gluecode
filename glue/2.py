import sys
import boto3
import datetime
from datetime import date
from datetime import datetime
from awsglue.transforms import *
from boto3.dynamodb.conditions import Key, Attr
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
import logging

args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
args = getResolvedOptions(sys.argv, ['JOB_NAME','job_name','run_date'])
job.init(args['JOB_NAME'], args)

#Init parameter
# Dynamo db table for checkpoints
job_name = args['job_name']  #"PERSON_TBL" #paramter

#run date YYYY-MM-DD
today = date.today()
run_date = args['run_date'] #today.strftime('%Y-%m-%d') # param

#Global variable
batch_id =0

# Last modified column for checkpointing in DynamoDB
lastModifiedColumn = 'id'

#Get lobal variable from Dynamodb master table
dynamodb = boto3.client('dynamodb', region_name='us-east-2')
response = dynamodb.get_item(TableName="JOB_MASTER_DATA",
                              Key={'JOB_NAME': {'S': str(job_name)}
                              })

#Get Catalog Connect
x =response['Item']['CATALOG_CON']
catalog_conn = list(x.values())[0]

#Get region
x =response['Item']['REGION']
region = list(x.values())[0]

#Get Redshift temp
x =response['Item']['REDSHIFT_TEMP']
redshift_temp  = list(x.values())[0]

#Get dynamo table of job
x =response['Item']['JOB_NAME']
dynamo_tbl = list(x.values())[0]

x =response['Item']['SRC_DBNAME']
src_databaseName = list(x.values())[0]

x =response['Item']['SRC_TBLNAME']
src_tableName = list(x.values())[0]

x =response['Item']['TRG_DBNAME']
tar_databaseName = list(x.values())[0]

x =response['Item']['TRG_TBLNAME']
tar_tablename = list(x.values())[0]

x =response['Item']['REDSHIFT_DBNAME']
redshift_dbname =  list(x.values())[0]

x =response['Item']['REDSHIFT_TBLNAME']
redshift_tblname =  list(x.values())[0]

x = response['Item']['MULTIPLE_RUN']
multiple_run = int(list(x.values())[0])

x = response['Item']['INCREMENTAL_LOAD']
load_type = int(list(x.values())[0]) # 0: full load, 1: incremental load

x = response['Item']['PRE_SCRIPT']
prescript = list(x.values())[0] # 0: full load, 1: incremental load

x = response['Item']['POST_SCRIPT']
postscript = list(x.values())[0] # 0: full load, 1: incremental load


# Create dynamodb table
def createDynamodbTable():
    table = dynamodb.create_table(TableName=dynamo_tbl,
                                  KeySchema=[{'AttributeName': 'BATCH_ID'
                                  , 'KeyType': 'HASH'}],
                                  AttributeDefinitions=[{'AttributeName': 'BATCH_ID'
                                  , 'AttributeType': 'S'}],
                                  ProvisionedThroughput={'ReadCapacityUnits': 5,
                                  'WriteCapacityUnits': 5})

    # Wait for the table to exist before exiting
    print ('Waiting for', dynamo_tbl, '...')
    waiter = dynamodb.get_waiter('table_exists')
    waiter.wait(TableName=dynamo_tbl)


# Function: Insert new item of job in DynamoDB
def insertCheckPoint(
    client,
    batch_id,
    run_date,
    lastcheckpoint,
    src_count,
    trg_count,
    status,
    error_msg
    ):

    start_run = datetime.now()
    start_run_string = start_run.strftime("%d/%m/%Y %H:%M:%S")
    response = client.put_item(TableName=dynamo_tbl,
                               Item={'BATCH_ID': {'S': batch_id},
                                     'RUN_DATE': {'S': run_date},
                                     'LAST_CHECKPOINT': {'S': lastcheckpoint},
                                     'SOURCE_COUNT': {'S': src_count},
                                     'TARGET_COUNT': {'S': trg_count},
                                     'PROCESS_STATUS': {'S': status},
                                     'ERROR_MSG': {'S': error_msg},
                                     'START_RUN': {'S': start_run_string},
                                     'END_RUN': {'S': start_run_string},
                                     'STAGING_STAGE': {'S': str(0)},
                                     'DATAMART_STAGE': {'S': str(1)}
                                    }
                              )
    return True

# Function: Update status in dynamodb
def updateCheckPoint(
    client,
    batch_id,
    run_date,
    lastcheckpoint,
    src_count,
    trg_count,
    status,
    error_msg,
    staging_stage,
    datamart_stage
   ):
    
    end_run = datetime.now()
    start_end_string = end_run.strftime("%d/%m/%Y %H:%M:%S")
    
    response = client.update_item(
                                  TableName=dynamo_tbl,  
                                  Key = {
                                     'BATCH_ID': {'S': str(batch_id)} 
                                  },
                                  UpdateExpression="SET RUN_DATE=:p_rundate, \
                                                        LAST_CHECKPOINT=:p_lastcheckpoint, \
                                                        SOURCE_COUNT=:p_sourcecount, \
                                                        TARGET_COUNT=:p_targetcount, \
                                                        PROCESS_STATUS=:p_status, \
                                                        ERROR_MSG=:p_error_msg, \
                                                        STAGING_STAGE=:p_staging_stage, \
                                                        DATAMART_STAGE=:p_datamart_stage, \
                                                        END_RUN=:p_end_run",
                                  ExpressionAttributeValues={
                                                     ':p_rundate': {'S':str(run_date)},
                                                     ':p_lastcheckpoint': {'S':str(lastcheckpoint)},
                                                     ':p_sourcecount': {'S':str(src_count)},
                                                     ':p_targetcount': {'S':str(trg_count)},
                                                     ':p_status': {'S':str(status)},
                                                     ':p_error_msg': {'S':str(error_msg)}, 
                                                     ':p_staging_stage': {'S':str(staging_stage)},
                                                     ':p_datamart_stage': {'S':str(datamart_stage)},
                                                     ':p_end_run': {'S':str(start_end_string)}
                                                     
                                  },
                                  ReturnValues="UPDATED_NEW"
                             )
    return True

# check if any run for today  
def checkCurrentRun(
    run_date):
    
    var_staging_stage = 0
    var_datamart_stage = 0
    var_process_status = 0
    var_batch_id = 0
    
    dynamodb = boto3.resource('dynamodb', region_name=region)
    table = dynamodb.Table(dynamo_tbl)
    
    fe = Key('RUN_DATE').eq(run_date)#&Key('PROCESS_STATUS').eq('SUCCESS')
    pe = "BATCH_ID,PROCESS_STATUS,STAGING_STAGE,DATAMART_STAGE"
    
    response = table.scan(
        FilterExpression=fe,
        ProjectionExpression=pe
        )
    
    if len(response['Items']) == 0: #Not run in current date
        flag = False 
        var_staging_stage = 0
        var_datamart_stage =0
        var_process_status =0
        var_batch_id = 0
    else:
        #Get latest BATCH ID in run date
        id = -1
        for x in response['Items']:
            if int(x['BATCH_ID']) > int(id):
                id = int(x['BATCH_ID'])
                item = x
        flag = True
        
        var_staging_stage = int(item['STAGING_STAGE'])
        var_datamart_stage = int(item['DATAMART_STAGE'])
        var_process_status = item['PROCESS_STATUS']
        var_batch_id = int(item['BATCH_ID'])
    return flag, var_batch_id, var_staging_stage, var_datamart_stage, var_process_status

def getLastBatchId():
    dynamodb = boto3.resource('dynamodb', region_name=region)
    table = dynamodb.Table(dynamo_tbl)
    pe = "BATCH_ID"
    
    response = table.scan(
         ProjectionExpression=pe
        )
    
    if len(response['Items']) == 0: #Not exists any previouss run
        id =0
    else:
        id = -1
        for i in response['Items']:
            if int(i['BATCH_ID']) > int(id):
                id = int(i['BATCH_ID'])
    return int(id)

def doMapping(df_Source, df_Target):
    #Convert tupe to dictionary
    mapping = []
    source_schema = dict(df_Source.toDF().dtypes)
    target_schema = dict(df_Target.toDF().dtypes)
    for key, value in target_schema.items():
        source_name = key
        source_type = value
        if target_schema.get(key) is not None:
            target_name = key
            target_type = target_schema[key]
            mapping.append((
            source_name,
            source_type,
            target_name,
            target_type
        ))
    return mapping

# Function: get key 
def getLastCheckPoint(
    batch_id):
    
    
    dynamodb = boto3.resource('dynamodb', region_name=region)
    table = dynamodb.Table(dynamo_tbl)
    fe = Key('BATCH_ID').lt(batch_id)&Key('PROCESS_STATUS').eq('SUCCESS')
    pe = "LAST_CHECKPOINT"

    response = table.scan(
        FilterExpression=fe,
        ProjectionExpression=pe
        )
    
    if len(response['Items']) == 0: #Not exists any previouss run
        checkpoint =0
    else:
        checkpoint = -1
        for i in response['Items']:
            if int(i['LAST_CHECKPOINT']) > checkpoint:
                checkpoint = i['LAST_CHECKPOINT']
    return checkpoint   

# Function: get key 
def checkKeyExist(
    client,
    batch_id):
    
    response = client.get_item(TableName=dynamo_tbl,
                              Key={'BATCH_ID': {'S': str(batch_id)}
                              })
    
    if 'Item' in response.keys(): #if keys exists
        flag = True 
    else:
        flag = False
    return flag




# Get an array of table names associated with the current account and endpoint.
response = dynamodb.list_tables()

if dynamo_tbl in response['TableNames']:
    table_found = True
else:
    createDynamodbTable()

#Will check if run today or not. 
# Will create entry to start new job if not run today yet
var_staging_stage = 0
var_datamart_stage = 0
var_process_status = ""
    
flag, batch_id, var_staging_stage, var_datamart_stage, var_process_status = checkCurrentRun(run_date)
if flag == True: #exist run date
    if ((var_staging_stage == 1) and (var_datamart_stage == 1) and (var_process_status =='SUCCESS')):
        if multiple_run == 0: #run only time in day (single run). Nothing to do and stop
            IsRunning = 0
            job.commit()
        else: #allow mutiple time in day
            batch_id = int(getLastBatchId())
            batch_id = int(batch_id) + 1
            var_staging_stage = 0
            var_datamart_stage = 0
            var_process_status = ""
            insertCheckPoint(dynamodb,str(batch_id),run_date,str(0),str(0),str(0),'START',str(''))
            IsRunning = 1
    else: #rerun job in that day because it has not finished one of steps
        
        IsRunning = 1 # run with old batch_id
else: #not exist run date
    batch_id = int(getLastBatchId())
    batch_id = int(batch_id) + 1
    var_staging_stage = 0
    var_datamart_stage = 0
    var_process_status = ""
    insertCheckPoint(dynamodb,str(batch_id),run_date,str(0),str(0),str(0),'START',str(''))
    IsRunning = 1


if IsRunning==1:
    if var_staging_stage==0:
        try:
            ## @type: DataSource
            ## @args: [database = "postgres", table_name = "ticket_dms_sample_person", transformation_ctx = "DataSource0"]
            ## @return: DataSource0
            ## @inputs: []
            DataSource0 = glueContext.create_dynamic_frame.from_catalog(database = src_databaseName, table_name = src_tableName, transformation_ctx = "DataSource0")


            #Get target schema from Data catelog
            DataTarget = glueContext.create_dynamic_frame.from_catalog(database = tar_databaseName, table_name = tar_tablename, redshift_tmp_dir ="s3://ntdbatest")

            if load_type == 0: #full load
                last_checkpoint = 0 #because full load so last checkpoint always = 0
                mapping = doMapping(DataSource0,DataTarget)

                Transform0 = ApplyMapping.apply(frame = DataSource0, mappings = mapping, transformation_ctx = "Transform0")
                
                SQLString = """
                begin;
                    CALL """ + prescript+ """(""" + """'""" + dynamo_tbl + """'"""  + """,""" + str(batch_id) + """,""" + """'""" + run_date + """'""" +"""); 
                    commit;
                end;"""
                
                datasink0 = glueContext.write_dynamic_frame.from_jdbc_conf(
                                    frame = Transform0, 
                                    catalog_connection = catalog_conn, 
                                    connection_options = {"preactions":SQLString, "dbtable": redshift_tblname, "database": redshift_dbname}, 
                                    redshift_tmp_dir = redshift_temp, transformation_ctx = "datasink0")  

                src_count = DataSource0.count()
                trg_count = Transform0.count()
                
                updateCheckPoint(dynamodb,batch_id,run_date,last_checkpoint,str(src_count), str(trg_count), 'SUCCESS','',str('1'),str(var_datamart_stage))  
            else: #incremental load
                #Get the latest Id of previous run
                last_checkpoint = getLastCheckPoint(batch_id)

                #Filter data after last checkpoint
                incremental_dyf = Filter.apply(frame=DataSource0, 
                                            f=lambda x: x[lastModifiedColumn] > int(last_checkpoint))
                number_of_rows = incremental_dyf.count()

                if number_of_rows > 0:
                    ## @type: ApplyMapping
                    Transform0 = ApplyMapping.apply(frame = incremental_dyf, mappings = mapping, transformation_ctx = "Transform0")

                    # Find New Checkpoint
                    df1 = Transform0.toDF()
                    last_checkpoint = int(df1.agg({lastModifiedColumn: 'max'}).collect()[0][0])   

                    #Insert into Target
                    SQLString = """
                    begin;
                        CALL """ + prescript+ """(""" + """'""" + dynamo_tbl + """'"""  + """,""" + str(batch_id) + """,""" + """'""" + run_date + """'""" +"""); 
                        commit;
                    end;"""

                    datasink0 = glueContext.write_dynamic_frame.from_jdbc_conf(
                                    frame = Transform0, 
                                    catalog_connection = catalog_conn, 
                                    connection_options = {"preactions":SQLString, "dbtable": redshift_tblname, "database": redshift_dbname}, 
                                    redshift_tmp_dir =redshift_temp, transformation_ctx = "datasink0")  


                    src_count = Transform0.count()
                    trg_count = datasink0.count()

                    updateCheckPoint(dynamodb,batch_id,run_date,last_checkpoint,str(src_count), str(trg_count), 'SUCCESS','',str('1'),str(var_datamart_stage))  
        except:
            MSG_FORMAT = '%(asctime)s %(levelname)s %(name)s: %(message)s'
            DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
            logging.basicConfig(format=MSG_FORMAT, datefmt=DATETIME_FORMAT)
            logger = logging.getLogger("Error")
            logger.info("Error message")
            updateCheckPoint(dynamodb,batch_id,run_date,last_checkpoint,str(src_count), str(trg_count), 'FAILED',"error_message",str('0'),str(var_datamart_stage)) 
    if var_datamart_stage==0:
        print('Do It later')

job.commit()
